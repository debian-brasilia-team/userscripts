## Debian Bugs

Adds a better visual to [bugs.debian.org](bugs.debian.org) webpage using `boostrap`.

Version: `0.1.0`

- [**Install**](https://salsa.debian.org/debian-brasilia-team/userscripts/-/raw/main/scripts/bugs/main.user.js)

---

#### Screenshot

![demo](images/demo.png)