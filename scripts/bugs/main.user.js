// ==UserScript==
// @name         Debian Bugs
// @namespace    http://debianbsb.org/userscripts
// @version      0.1.0
// @description  Adds a better visual to bugs.debian.org webpage using boostrap.
// @author       Arthur Diniz <arthurbdiniz@gmail.com>
// @match        https://bugs.debian.org/cgi-bin/bugreport.cgi*
// @icon         https://www.debian.org/favicon.ico
// @updateURL    https://salsa.debian.org/debian-brasilia-team/userscripts/-/raw/main/scripts/bugs/main.user.js
// @downloadURL  https://salsa.debian.org/debian-brasilia-team/userscripts/-/raw/main/scripts/bugs/main.user.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // ---------------------------------------------------

    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = 'https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css';

    document.head.appendChild(link);

    // ---------------------------------------------------

    //var bodyContent = document.body.innerHTML;
    var bodyElement = document.body;
    var bugInfo = document.getElementsByClassName("buginfo")[0];
    var pgkInfo = document.getElementsByClassName("pkginfo")[0];
    var versiongraph = document.getElementsByClassName("versiongraph")[0];

    // ---------------------------------------------------

    var styleElement = document.createElement('style');
    styleElement.type = 'text/css';
    document.head.appendChild(styleElement);

    // Add CSS rule to override the float property
    styleElement.sheet.insertRule('.versiongraph { float: none !important; }');

    var containerDiv = document.createElement('div');
    containerDiv.className = 'container mt-4';

    var col8Div = document.createElement('div');
    col8Div.className = 'col-9';
    while (bodyElement.firstChild) {
        col8Div.appendChild(bodyElement.firstChild);
    }

    var col4Div = document.createElement('div');
    col4Div.className = 'col-3';
    col4Div.append(versiongraph);
    col4Div.appendChild(document.createElement('hr'));
    col4Div.append(bugInfo);
    col4Div.appendChild(document.createElement('hr'));
    col4Div.append(pgkInfo);
    col4Div.appendChild(document.createElement('hr'));

    var rowDiv = document.createElement('div');
    rowDiv.className = 'row';
    rowDiv.appendChild(col8Div);
    rowDiv.appendChild(col4Div);

    containerDiv.appendChild(rowDiv)

    document.body.innerHTML = ''; // Clear the body
    document.body.appendChild(containerDiv);

    var aElements = document.getElementsByTagName("a");

    for (var i = 0; i < aElements.length; i++) {
        if (aElements[i].innerText.includes("Reply")) {
            aElements[i].classList.add("btn");
            aElements[i].classList.add("btn-primary");
            aElements[i].style.color = "white";
        }
        if (aElements[i].innerText.includes("subscribe")) {
            aElements[i].classList.add("btn");
            aElements[i].classList.add("btn-primary");
            aElements[i].style.color = "white";
        }
    }

    var h1Elements = document.querySelectorAll('h1');

    for (let i = 0; i < h1Elements.length; i++) {
        if (h1Elements[i].innerText.trim().startsWith("Debian Bug report logs - ")) {
            let aElement = h1Elements[i].querySelector('a');
            col4Div.append(aElement);

            h1Elements[i].innerText = h1Elements[i].innerText.replace('Debian Bug report logs -', '');
            var h2Element = document.createElement('h2');
            h2Element.innerHTML = h1Elements[i].innerHTML;
            h1Elements[i].parentNode.replaceChild(h2Element, h1Elements[i]);
        }
    }

    let preElements = document.querySelectorAll('pre.message');
    for (let i = 0; i < preElements.length; i++) {
        preElements[i].classList.add('card', 'p-3');
    }

    let pElements = document.getElementsByTagName('p');
    for (let i = 0; i < pElements.length; i++) {
        let links = pElements[i].getElementsByTagName('a');
        for (let j = 0; j < links.length; j++) {
            if (links[j].classList.contains('btn') &&
                links[j].classList.contains('btn-primary') &&
                links[j].style.color === 'white') {
                col4Div.append(pElements[i]);
            }
        }
    }

})();
