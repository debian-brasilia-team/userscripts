# userscripts

### Step 1: install a user script manager

To use user scripts you need to first install a user script manager. Which user script manager you can use depends on which browser you use.


- [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/)
- [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)
- [Violentmonkey](https://addons.mozilla.org/firefox/addon/violentmonkey/)

---

### Step 2: install a user script

#### Scripts

- [Debian Bugs](scripts/bugs/README.md)

---

### Step 3: use the user script

Go to the site the user script affects. It should automatically do its thing.
